import React from 'react';
import Logo from '../../components/Logo';
import HeroHead from './Hero/Head';
import HeroBody from './Hero/Body';
import HeroFoot from './Hero/Foot';
import './styles.css';

const Main = ({ db, pkg }) => {
    return (
        <section className="hero is-info is-fullheight">
            <HeroHead logo={<Logo />} pkg={pkg} />
            <HeroBody db={db} />
            <HeroFoot />
        </section>
    );
};

export default Main;
