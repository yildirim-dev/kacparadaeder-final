import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

const HeroHead = ({ logo, pkg }) => {
    const {
        repository: { url: repoUrl },
    } = pkg;

    return (
        <div className="hero-head">
            <header className="navbar">
                <div className="container">
                    <div className="navbar-brand">
                        <a className="navbar-item is-active" href="/">
                            {logo}
                        </a>
                        <span className="navbar-burger" data-target="navbarMenuHeroC">
                            <span></span>
                            <span></span>
                            <span></span>
                        </span>
                    </div>
                    <div id="navbarMenuHeroC" className="navbar-menu">
                        <div className="navbar-end">
                            {repoUrl && (
                                <div className="navbar-item ">
                                    <div className="buttons">
                                        <a href={repoUrl} target="_blank" rel="noreferrer" className="button is-light">
                                            <span className="icon">
                                                <FontAwesomeIcon icon="code" />
                                            </span>
                                            <span>Kaynak Kodlar</span>
                                        </a>
                                    </div>
                                </div>
                            )}
                        </div>
                    </div>
                </div>
            </header>
        </div>
    );
};

export default HeroHead;
