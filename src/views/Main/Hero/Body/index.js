import React, { useState } from 'react';
import { ReactSearchAutocomplete } from 'react-search-autocomplete';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import ToggleButton from '../../../../components/ToggleButton';
import './styles.css';

const HeroBody = ({ db }) => {
    const { attrList, currency, questions, urunler } = db;

    const items = urunler.map((u) => ({ ...u, display: `${u.marka} ${u.model}` }));

    const [selectedItem, setSelectedItem] = useState(null);
    const [attrs, setAttrs] = useState(Object.fromEntries(attrList.map((att) => [att, null])));
    const [qIndex, setQIndex] = useState(0);

    const handleOnSelect = (item) => {
        resetAll();
        setTimeout(() => {
            setSelectedItem(item);
        }, 100);
    };

    const handleOnClear = () => {
        resetAll();
    };

    const resetAll = () => {
        resetSelectedItem();
        resetAttrs();
        resetQIndex();
    };

    const resetSelectedItem = () => {
        setSelectedItem(null);
    };

    const resetAttrs = () => {
        setAttrs(Object.fromEntries(attrList.map((att) => [att, null])));
    };

    const resetQIndex = () => {
        setQIndex(0);
    };

    const nextQuestion = () => {
        const nextIndex = qIndex < questions.length ? qIndex + 1 : questions.length - 1;
        setQIndex(nextIndex);
    };

    const prevQuestion = () => {
        const nextIndex = qIndex > 0 ? qIndex - 1 : 0;
        setQIndex(nextIndex);
    };

    const getHandlerForAttr = (attr, val) => {
        return () => {
            if (attrs[attr] === null) {
                nextQuestion();
                setAttrs({ ...attrs, [attr]: val });
            } else {
                prevQuestion();
                setAttrs({ ...attrs, [attr]: null });
            }
        };
    };

    const getStatusForAttr = (attr) => {
        if (attrs[attr] === false) {
            return `- ${currency} ${selectedItem[attr]}`;
        } else if (attrs[attr] === true) {
            return <FontAwesomeIcon icon="check" />;
        } else {
            return <FontAwesomeIcon icon="spinner" className="fa-pulse" />;
        }
    };

    const getItemPriceDefault = (item) => {
        return `${currency} ${item['fiyat']}`;
    };

    const getItemPriceFinal = (item) => {
        let price = item['fiyat'];
        attrList.forEach((att) => {
            if (attrs[att] === false) price -= item[att];
        });
        return `${currency} ${price}`;
    };

    const getProgress = () => {
        return parseInt((qIndex / questions.length) * 100);
    };

    return (
        <div className="hero-body">
            <div className="container">
                <p className="title has-text-centered">
                    Telefonunuz <span className="logo-inline">KaçParaEder</span> hemen öğrenin!
                </p>
                <div className="columns is-centered">
                    <div className="column is-8">
                        <ReactSearchAutocomplete
                            items={items}
                            resultStringKeyName="display"
                            fuseOptions={{ keys: ['marka', 'model'] }}
                            onSelect={handleOnSelect}
                            onClear={handleOnClear}
                            autoFocus={true}
                            className="input is-medium is-rounded"
                            placeholder="Yazmaya başlayın..."
                            styling={{
                                zIndex: 1000,
                            }}
                        />
                    </div>
                </div>
                {selectedItem && (
                    <div>
                        <div className="columns is-centered">
                            <div className="column is-8">
                                <div className="card frosted-glass">
                                    <div className="card-content">
                                        <div className="content">
                                            <div className="columns is-vcentered">
                                                <div className="column">
                                                    <span>Temel Fiyat</span>
                                                </div>
                                                <div className="column is-2">
                                                    <span>{getItemPriceDefault(selectedItem)}</span>
                                                </div>
                                            </div>
                                            <hr />
                                            {questions.map(({ text, attr }, i) => (
                                                <div
                                                    className="columns is-vcentered question-line"
                                                    key={`question-${i}`}
                                                    style={{
                                                        height: !(qIndex >= i) && 0,
                                                        opacity: !(qIndex >= i) && 0,
                                                    }}
                                                >
                                                    <div className="column">
                                                        <span>{text}</span>
                                                    </div>
                                                    <div className="column">
                                                        <ToggleButton
                                                            disabled={qIndex !== i}
                                                            yesHandler={getHandlerForAttr(attr, true)}
                                                            noHandler={getHandlerForAttr(attr, false)}
                                                        />
                                                    </div>
                                                    <div className="column is-2">
                                                        <span>{getStatusForAttr(attr)}</span>
                                                    </div>
                                                </div>
                                            ))}
                                            {qIndex === questions.length && (
                                                <div>
                                                    <hr />
                                                    <div className="columns is-vcentered final-price">
                                                        <div className="column is-centered">
                                                            <span className="logo-inline">KaçParaEder?</span>
                                                        </div>
                                                        <div className="column is-2">
                                                            <span className="has-background-success final-price-badge">
                                                                {getItemPriceFinal(selectedItem)}
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            )}
                                        </div>
                                    </div>
                                    <progress className="progress is-small is-success" value={getProgress()} max="100">
                                        {getProgress()}%
                                    </progress>
                                </div>
                            </div>
                        </div>
                        <div className="columns is-centered is-vcentered">
                            <div className="column is-1 is-centered">
                                <button className="button reload-button" onClick={() => window.location.reload()}>
                                    <span className="icon">
                                        <FontAwesomeIcon icon="redo" />
                                    </span>
                                    <span>Sıfırla</span>
                                </button>
                            </div>
                        </div>
                    </div>
                )}
            </div>
        </div>
    );
};

export default HeroBody;
