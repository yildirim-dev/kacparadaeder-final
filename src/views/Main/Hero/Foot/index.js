import React from 'react';
import './styles.css';

const HeroFoot = () => {
    return (
        <div className="hero-foot">
            <footer>
                <div className="content has-text-centered">
                    <p>&copy; {new Date().getFullYear()} KaçParaEder?</p>
                </div>
            </footer>
        </div>
    );
};

export default HeroFoot;
