import React from 'react';
import ReactDOM from 'react-dom';
import 'bulma/css/bulma.min.css';
import { library } from '@fortawesome/fontawesome-svg-core';
import { fab } from '@fortawesome/free-brands-svg-icons';
import { faCheck, faCode, faRedo, faSpinner } from '@fortawesome/free-solid-svg-icons';
import Home from './views/Main';
import db from './data/db.json';
import pkg from '../package.json';

library.add(fab, faCheck, faCode, faRedo, faSpinner);

ReactDOM.render(
    <React.StrictMode>
        <Home db={db} pkg={pkg} />
    </React.StrictMode>,
    document.getElementById('root'),
);
