import React from 'react';
import './styles.css';

const Logo = ({ text = 'KaçParaEder?' }) => {
    return (
        <div className="logo">
            <span>{text}</span>
        </div>
    );
};

export default Logo;
