import React, { useState } from 'react';

const ToggleButton = ({ yesHandler, noHandler, disabled = false }) => {
    const [selected, setSelected] = useState('');

    const handleClick = (btn) => {
        btn.handler();
        if (btn.tag === selected) {
            setSelected('');
            return;
        }
        setSelected(btn.tag);
    };

    const buttons = [
        {
            tag: 'yes',
            text: 'Evet',
            class: 'is-success',
            handler: yesHandler,
        },
        {
            tag: 'no',
            text: 'Hayır',
            class: 'is-danger',
            handler: noHandler,
        },
    ];

    return (
        <div className="buttons has-addons is-centered">
            {buttons.map((btn) => (
                <button
                    key={`btn-${btn.tag}`}
                    onClick={() => handleClick(btn)}
                    className={`button is-rounded ${selected === btn.tag && btn.class + ' is-selected'}`}
                    disabled={disabled}
                >
                    {btn.text}
                </button>
            ))}
        </div>
    );
};

export default ToggleButton;
