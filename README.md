# Kaç Para Eder?

## Nasıl Kullanılır?

-   Gereksinimleri yükleyin

    ```bash
    npm install
    ```

-   Ardından aşağıdaki komutları kullanabilirsiniz.

## Kullanılabilir komutlar

### `npm start`

Uygulamayı geliştirme modunda çalıştırır.\
Tarayıcınızda [http://localhost:3000](http://localhost:3000) adresine giderek uygulamayı görebilirsiniz.

Eğer kodlarda değişiklik yaparsanız sayfa kendini yenileyecektir.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.
